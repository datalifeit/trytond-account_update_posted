# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    @ModelView.button
    def draft(cls, moves):
        for move in moves:
            if not move.state == 'posted':
                continue
            if not move.journal.update_posted:
                raise UserError(gettext(
                    'account_update_posted.msg_draft_posted_move_journal',
                    move=move.rec_name,
                    journal=move.journal.rec_name))
            if move.period.state == 'closed':
                raise UserError(gettext(
                    'account_update_posted.msg_draft_closed_period',
                    move=move.rec_name,
                    period=move.period.rec_name))
        super(Move, cls).draft(moves)


class MoveBankStatement(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    @ModelView.button
    def draft(cls, moves):
        BankMove = Pool().get('account.bank.reconciliation')

        super(Move, cls).draft(moves)

        delete_bank_lines = []
        for move in moves:
            for line in move.lines:
                delete_bank_lines += [x for x in line.bank_lines]
        BankMove.delete(delete_bank_lines)
