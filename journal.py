# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Journal(metaclass=PoolMeta):
    __name__ = 'account.journal'

    update_posted = fields.Boolean('Allow updating posted moves')

    @staticmethod
    def default_update_posted():
        return False
